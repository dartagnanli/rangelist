package main

import (
	"fmt"
	"sort"
)

// Task: Implement a struct named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
// NOTE: Feel free to add any extra member variables/functions you like.

type RangeList struct {
	m  map[int]int
	oa Array
}

// 计算区间
func (rangeList *RangeList) find(left, right int) (int, int) {
	l, r := rangeList.oa.lowerBound(left+1), rangeList.oa.lowerBound(right+1)
	if l != 0 {
		l--
		if rangeList.m[rangeList.oa.get(l)] < left {
			l++
		}
	}
	if l == r {
		return left, right
	}
	left = min(left, rangeList.oa.get(l))
	right = max(right, rangeList.m[rangeList.oa.get(r-1)])
	for i := l; i < r; i++ {
		delete(rangeList.m, rangeList.oa.get(i))
	}
	rangeList.oa.removeItem(l, r)
	return left, right
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	left, right := getElement(rangeElement)
	l, r := rangeList.find(left, right)
	if _, ok := rangeList.m[l]; !ok {
		rangeList.oa.addItem(l)
	}
	rangeList.m[l] = r
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	left, right := getElement(rangeElement)
	l, r := rangeList.find(left, right)
	if left > l {
		if _, ok := rangeList.m[l]; !ok {
			rangeList.oa.addItem(l)
		}
		rangeList.m[l] = left
	}
	if right < r {
		if _, ok := rangeList.m[right]; !ok {
			rangeList.oa.addItem(right)
		}
		rangeList.m[right] = r
	}
	return nil
}

// Print 排序输出
func (rangeList *RangeList) Print() error {
	var res string
	var keys []int
	for key := range rangeList.m {
		keys = append(keys, key)
	}
	// 使用快速排序
	sort.Ints(keys)
	for _, key := range keys {
		res += fmt.Sprint("[", key, ",", rangeList.m[key], ")")
	}
	println(res)
	return nil
}

func getElement(rangeElement [2]int) (int, int) {
	return rangeElement[0], rangeElement[1]
}

func min(a, b int) int {
	if a <= b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a <= b {
		return b
	}
	return a
}

func main() {
	rl := RangeList{
		m: map[int]int{},
		oa: Array{
			arr: []int{},
		},
	}

	rl.Add([2]int{1, 5})
	rl.Print()
	// Should display: [1, 5)
	rl.Add([2]int{10, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 20})
	rl.Print()
	// Should display: [1, 5) [10, 20)
	rl.Add([2]int{20, 21})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{2, 4})
	rl.Print()
	// Should display: [1, 5) [10, 21)
	rl.Add([2]int{3, 8})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 10})
	rl.Print()
	// Should display: [1, 8) [10, 21)
	rl.Remove([2]int{10, 11})
	rl.Print()
	// Should display: [1, 8) [11, 21)
	rl.Remove([2]int{15, 17})
	rl.Print()
	// Should display: [1, 8) [11, 15) [17, 21)
	rl.Remove([2]int{3, 19})
	rl.Print()
	// Should display: [1, 3) [19, 21)
}
