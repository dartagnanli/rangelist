// Package main implements
package main

type Array struct {
	arr []int
}

// 向数组中添加满足条件的区间
func (arr *Array) addItem(item int) {
	i := arr.lowerBound(item)
	if i != len(arr.arr) {
		arr.arr = append(arr.arr, 0)
		copy(arr.arr[i+1:], arr.arr[i:])
		arr.arr[i] = item
	} else {
		arr.arr = append(arr.arr, item)
	}
}

// 二分查找，返回左边际值下标
func (arr *Array) lowerBound(item int) int {
	i, j := 0, len(arr.arr)
	for i < j {
		h := i + (j-i)>>1
		if arr.arr[h] < item {
			i = h + 1
		} else {
			j = h
		}
	}
	return i
}

func (arr *Array) get(index int) int {
	return arr.arr[index]
}

// 移除区间
func (arr *Array) removeItem(left, right int) {
	if right != len(arr.arr) {
		copy(arr.arr[left:], arr.arr[right:])
	}
	arr.arr = arr.arr[:len(arr.arr)-(right-left)]
}
